#coding:utf-8
import random
import urllib2

random.seed(1)

url = "http://fififactory.sakura.ne.jp/API/index.cgi?player="
hand = ["g","p","c"]
def judge(p):
    ROCK = 0
    PAPAER = 0
    SCISSORS = 0
    t = []
    for i in p:

        print "hand:" + str(i.x)

    print ""

    for i in p:
        if i.x == 0:
            ROCK += 1
        elif i.x == 1:
            PAPAER += 1
        elif i.x == 2:
            SCISSORS += 1
    print ROCK,PAPAER,SCISSORS
    if (ROCK and PAPAER and SCISSORS) or\
       (ROCK and not PAPAER and not SCISSORS) or\
       (not ROCK and PAPAER and not SCISSORS) or\
       (not ROCK and not PAPAER and SCISSORS):
        for i in p:
            i.draw += 1
            t.append(i)


    elif ROCK and SCISSORS and not PAPAER:
        #グーの人勝ち
        for i in p:
            if i.x == 0:
                i.winR += 1
            elif i.x == 2:
                i.loseS += 1
            t.append(i)

    elif ROCK and not SCISSORS and  PAPAER:
        #パーの人勝ち
        for i in p:
            if i.x == 1:
                i.winP += 1
            elif i.x == 0:
                i.loseR += 1
            t.append(i)

    elif not ROCK and SCISSORS and PAPAER:
        #チョキの人勝ち
        for i in p:
            if i.x == 2:
                i.winS += 1
            elif i.x == 1:
                i.loseP += 1
            t.append(i)

    return t

def judge_kai(p):
    ROCK = 0
    PAPAER = 0
    SCISSORS = 0
    t = []
    p[1].enemy_hand += hand[p[0].x]
    print p[1].enemy_hand
    for i in p:
        if i.x == 0:
            ROCK += 1
        elif i.x == 1:
            PAPAER += 1
        elif i.x == 2:
            SCISSORS += 1
    if (ROCK and PAPAER and SCISSORS) or\
       (ROCK and not PAPAER and not SCISSORS) or\
       (not ROCK and PAPAER and not SCISSORS) or\
       (not ROCK and not PAPAER and SCISSORS):
        for i in p:
            i.draw += 1
            t.append(i)


    elif ROCK and SCISSORS and not PAPAER:
        #グーの人勝ち
        for i in p:
            if i.x == 0:
                i.winR += 1
            elif i.x == 2:
                i.loseS += 1
            t.append(i)

    elif ROCK and not SCISSORS and  PAPAER:
        #パーの人勝ち
        for i in p:
            if i.x == 1:
                i.winP += 1
            elif i.x == 0:
                i.loseR += 1
            t.append(i)

    elif not ROCK and SCISSORS and PAPAER:
        #チョキの人勝ち
        for i in p:
            if i.x == 2:
                i.winS += 1
            elif i.x == 1:
                i.loseP += 1
            t.append(i)

    return t



class Person:
    def __init__(self):
        self.x = random.randint(0,2)
        self.draw = 0
        self.winR = 0
        self.winP = 0
        self.winS = 0
        self.loseR = 0
        self.loseP = 0
        self.loseS = 0
        self.enemy_hand = ""
    def status(self):
        print "draw:"+str(self.draw)
        print "winR:"+str(self.winR)
        print "winP:"+str(self.winP)
        print "winS:"+str(self.winS)
        print "loseR:"+str(self.loseR)
        print "loseP:"+str(self.loseP)
        print "loseS:"+str(self.loseS)
        print
    def change_hands(self):
        self.x = random.randint(0,2)
    def read_hands(self):
        req = urllib2.Request(url+self.enemy_hand)
        response = urllib2.urlopen(req)
        page = response.read()
        index = str(page).find("\"next_hand\":")
        if index:
            print page[index+14]
            if page[index+14] == "g":
                self.x = 0
            elif page[index+14] == "p":
                self.x = 1
            elif page[index+14] == "c":
                self.x = 2
        print page
        print index

if __name__ == '__main__':
    import sys
    param = sys.argv
    print len(param)
    if len(param) > 2:
        num = int(param[1])
        x = int(param[2])
    else:
        print "Error parameter"
        print "python janken2.py <Person num> <Repeat>"
        sys.exit()
    p = []
    for i in range(num):
        p.append(Person())
    for i in range(x):
        p = judge_kai(p)
        for j in range(num):
            p[j].change_hands()
            p[1].read_hands()

    for i in p:
        i.status()
